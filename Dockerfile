FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

# passed from ./update.sh
ARG MKDOCS_MATERIAL_VERSION
ARG MKDOCS_REDIRECTS_VERSION
ARG SURFER_VERSION

RUN apt-get update && \
    apt install -y python3-setuptools && \
    pip3 install mkdocs-material==$MKDOCS_MATERIAL_VERSION && \
    pip3 install mkdocs-redirects==${MKDOCS_REDIRECTS_VERSION} && \
    pip3 install pillow cairosvg && \
    npm i -g @redocly/cli && \
    npm install -g cloudron-surfer@$SURFER_VERSION
