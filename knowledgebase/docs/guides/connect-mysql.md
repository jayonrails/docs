# Connect to MySQL Addon

## Overview

Apps on Cloudron use an internal MySQL database (addon). For security reasons, the database is only accessible
from inside the server and cannot be accessed directly from outside. Instead, the database is accessed using
an SSH tunnel to the server.

In this guide, we will see how to connect to the MySQL addon database from outside (say, your laptop/PC).

## Database credentials

To get the database credentials, open the [Web terminal](/apps/#web-terminal) of an app and
run `env | grep CLOUDRON_MYSQL`. Make a note of the credentials. Note that each app has it's own separate
database and thus it's own database credentials.

<center>
<img src="/guides/img/mysql-addon-env.png" class="shadow" width="500px">
</center>

## Internal IP Address

The internal IP address of the MySQL server is `172.18.30.1`.

## DB Clients

### CLI

To connect via MySQL CLI, open a [Web Terminal](https://docs.cloudron.io/apps/#web-terminal) and click
the `MySQL` button at the top. This will paste the CLI connection command into the Web Terminal. Press enter to execute
the command and use the CLI.

### DBeaver

[DBeaver](https://dbeaver.io) is a free multi-platform database tool for developers, database administrators.
You can download it [here](https://dbeaver.io/download/).

Create a new MySQL connection in DBeaver.

Fill up the values gathered in the steps above:

<center>
<img src="/guides/img/dbeaver-mysql-connection.png" class="shadow" width="500px">
</center>

Configure SSH tunnel by clicking on the SSH tab:

<center>
<img src="/guides/img/dbeaver-mysql-ssh-tunnel.png" class="shadow" width="500px">
</center>

You should now be able to view the database:

<center>
<img src="/guides/img/dbeaver-mysql-tables.png" class="shadow" width="500px">
</center>

### MySQL Workbench

[MySQL Workbench](https://www.mysql.com/products/workbench/) is a unified visual tool for database architects, developers, and DBAs.

Create a new connection in MySQL workbench.

Select `Standard TCP/IP over SSH` as the connection method. Fill up the values gathered in the steps above:

<center>
<img src="/guides/img/mysql-workbench-mysql-connection.png" class="shadow" width="500px">
</center>

You should now be able to view the database:

<center>
<img src="/guides/img/mysql-workbench-mysql-tables.png" class="shadow" width="500px">
</center>

