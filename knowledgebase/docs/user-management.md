# User management

## Overview

Cloudron provides a central user directory that apps can use for authentication.  This feature
allows users to use the same username & password for logging in to apps.

When installing an app, you can choose if the app is to be configured to use the Cloudron user directory.
Disabling the integration can be beneficial when the app is meant to be used primarily by external users
(for example, a community chat or a public forum).

Cloudron is an [OIDC provider](#openid-connect) as well as a LDAP server. As such, some apps do not support
either of these authentication schemes and users have to be managed inside the app.

When Cloudron user directory integration is available for an app, the user management options will look like below:

<center>
<img src="/img/users-directory-integration-available.png" class="shadow" width="500px">
</center>

When `Leave user management to the app` is selected, the app's Cloudron SSO integration will be disabled
and all user management has to be carried from within the app. This is useful when the app primarily caters
to external users (like say a community chat).

When Cloudron user directory integration is unavailable for an app, the user management options look like below:

<center>
<img src="/img/users-directory-integration-unavailable.png" class="shadow" width="500px">
</center>

## Users

New users can be added with their email address from the `Users` menu.

<center>
<img src="/img/user-list.png" class="shadow" width="500px">
</center>

Click on `New User` to add a new user:

<center>
<img src="/img/users-add.png" class="shadow" width="500px">
</center>

They will receive an invite to sign up. Once signed up, they can access the apps they have been given access to.

To remove a user, simply remove them from the list. Note that the removed user cannot access any app anymore.

### Valid usernames

The following characters are allowed in usernames:

* Alphanumeric characters
* '.' (dot)
* '-' (hyphen)

Usernames must be chosen with care to accomodate the wide variety of apps that run on Cloudron.
For example, very generic words like `error`, `pull`, `404` might be reserved by apps.

## Groups

Groups provide a convenient way to group users. You can assign one or more groups to apps to restrict who can
access for an app.

You can create a group by using the `Groups` menu item.

<center>
<img src="/img/users-groups-list.png" class="shadow">
</center>

Click on `New Group` to add a new group:

<center>
<img src="/img/users-groups-add.png" class="shadow">
</center>


To set the access restriction use the app's configure dialog.

<center>
<img src="/img/app-configure-group-acl.png" class="shadow" width="500px">
</center>

### Valid group names

The following characters are allowed in group names:

* Alphanumeric characters
* '.' (dot)
* '-' (hyphen)

## Roles

Roles provide a way to restrict the permissions of a user. You can assign a role from the `Users` page.

<center>
<img src="/img/users-role.png" class="shadow" width="500px">
</center>

### User

A Cloudron user can login to the Cloudron dashboard and use the apps that they have access to.
They can edit their profile (name, password, avatar) on the dashboard.

To allow a user to configure and manage specific apps, see the [App Operator](/apps/#operators) feature.

### User Manager

A User Manager can add, edit and remove users & groups. Newly added users always get the `User` role.
User Manager cannot modify the role of an existing user.

### Mail Manager

A Mail Manager can add, edit and remove mailboxes and mailing lists, in addition to being able to manage users.

!!! note "No access to mail server logs"
    For security reasons, a Mail Manager does not have access to the email server logs.

### Administrator

A Cloudron administrator can manage apps and users. Note that an admin can login to any app even
if they have not been explicitly granted access in the `Access Control` section.

If you only want to make a user configure and manage specific apps, use the [App Operator](/apps/#operators) feature.

### Superadmin

A Cloudron superadmin has the capabilities of administrator with the addition of the ability to manage the
Cloudron subscription, manage backup settings and configure branding.

A good way to think about the superadmin role is a person who is in charge of server administration and billing.

!!! note "Automatic login"
    When clicking the `Manage Subscription` button in the `Settings` view, they are automatically logged
    in to the cloudron.io account.

## Impersonate user

A common situation is to be able to pre-setup applications on behalf of a new user. In some cases, the user has
to login to an app first before they can be added to a channel/group/team or given specific permissions.

For such situations, the Cloudron admin can use the `Impersonate` button to generate a password that lets a Cloudron
login as another user. The password is time limited and can be used to login to the Cloudron dashboard and the apps.

<center>
<img src="/img/users-impersonate-button.png" class="shadow" width="500px">
</center>

Clicking the button will show a temporary password:

<center>
<img src="/img/users-impersonate-password.png" class="shadow" width="500px">
</center>

!!! note "Does not reset existing password"
    The temporary password does not overwrite the user's existing password.

## Password reset (link)

Users & Admins can reset their own passwords from the login screen. `https://my.example.com/login.html?passwordReset` is a direct link.

Admins can also email a password reset link for other users by clicking on the `Reset password` button.

<center>
<img src="/img/users-password-reset.png" class="shadow" width="500px">
</center>

This will open up a dialog showing the password reset link. If email delivery is not working for some
reason, the link can be sent to the user by some other means.

<center>
<img src="/img/users-copy-password-reset-link.png" class="shadow" width="500px">
</center>

## Password reset (ssh)

A temporary one-time use password for the superadmin account can be generated by SSHing into the server:

```
# sudo cloudron-support --owner-login
Login as superadminname / mW5x5do99TM2 . Remove /home/yellowtent/platformdata/cloudron_ghost.json when done.
```

The above password also bypasses any 2FA and can thus be used in situations where the superadmin has lost the 2FA device.

## Mandatory 2FA

Admins can require all users to set up two factor authentication by enabling the Mandatory 2FA setting.
To enable, use the setting in the `Users` view.

<center>
<img src="/img/users-mandatory-2fa.png" class="shadow" width="500px">
</center>

When enabled, all existing users without a 2FA setup are logged out immediately.

When users without 2FA attempt to login, they will be forced to setup 2FA:

<center>
<img src="/img/users-2fa-required.png" class="shadow" width="500px">
</center>

When the user clicks `Setup Two-Factor`, they go through the [2fa setup flow](/profile/#enabling-2fa):

<center>
<img src="/img/users-2fa-qrcode.png" class="shadow" width="500px">
</center>

## Disable 2FA

If a user loses their 2FA device, the Cloudron administrator can disable 2FA
in the user's edit dialog.

<center>
<img src="/img/users-2fa-reset.png" class="shadow" width="500px">
</center>

Once disabled, user can login with just their password. After login, they can
re-setup 2FA.

If the superadmin of Cloudron has lost their 2FA device, see the [password reset](#password-reset-ssh)
section to generate a one-time use password that will bypass 2FA.

## Disable user

To disable a user, uncheck the `User is active` option. Doing so, will invalidate all
existing Cloudron dashboard session of the user and will log them out. The user may
still be able to use any apps that they were logged into prior to the de-activation.
To log them out from the apps, you can check if the app provides a way to log them out
(support for this depends on the app).

<center>
<img src="/img/user-disable.png" class="shadow" width="500px">
</center>

!!! note "Disabling does not delete user data"
    Disable a user only blocks the login access for the user. Any data generated by the
    user inside apps is not deleted.

## Lock profile

Admins can disallow users from changing their email and full name by locking user profiles. To
lock the profile, simple uncheck the setting in the `Users` view.

<center>
<img src="/img/users-lock-profile.png" class="shadow" width="500px">
</center>

## Impersonate user

One can create a file named `/home/yellowtent/platformdata/cloudron_ghost.json` which contains an username
and a fake password like:

```
{"girish":"secret123"}
```

With such a file in place, you can login to the Webadmin UI using the above credentials
(the user has to already exist). This helps you debug and also look into how the UI might
look from that user's point of view.

## Export users

The user directory can be exposed in the CSV or JSON format using the `Export` button.

<center>
<img src="/img/users-export.png" class="shadow" width="500px">
</center>

The CSV contains the following fields:

```
id,username,email,fallback email,full name,role,active
```

The JSON format contains the above fields as an array of objects.

## Import users

Users can be imported into the user directory using the `Import` button.

The CSV contains the following fields:

```
username,email,fallback email,full name,role
```

<center>
<img src="/img/users-import.png" class="shadow" width="500px">
</center>

The JSON format contains the above fields as an array of objects.

## Directory Server

Cloudron can act as a LDAP server for apps hosted externally to Cloudron. External apps can then be configured
to list Cloudron users and allow users to authenticate with their Cloudron password.

You can enable the Directory Server from the `Users` view:

<center>
<img src="/img/users-directory-server.png" class="shadow" width="500px">
</center>

For security reasons, the LDAP server will only accept connections from specific white listed IPs and ranges.

The external app can be configured as follows:

* Use the dashboard domain as the LDAP server hostname. Port 636 (TLS).
* LDAP server uses the same certificate as the dashboard domain.
* Set `cn=admin,ou=system,dc=cloudron` as the Bind DN.
* Use the secret listed in the above screenshot as the Bind Password.

Users are listed under the `ou=users,dc=cloudron` search base. Groups are listed under the `ou=groups,dc=cloudron` search base.

The setup can be tested as follow:

```
$ ldapsearch  -x -b "ou=users,dc=cloudron" -D "cn=admin,ou=system,dc=cloudron" -W -H ldaps://my.example.com:636
Enter LDAP Password: 
# extended LDIF
#
# LDAPv3
# base <ou=users,dc=cloudron> with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# uid-0cfbd3d8-6547-4332-9415-dadfe8b78ac4, users, cloudron
dn: cn=uid-0cfbd3d8-6547-4332-9415-dadfe8b78ac4,ou=users,dc=cloudron
objectclass: user
objectclass: inetorgperson
objectclass: person
objectcategory: person
...
```

## External Directory

The External Directory connector allows users from your existing LDAP or active directory to authenticate with Cloudron.

The External Directory configuration is set in the `Users` view. 

Options:

* `Automatically create users when they login to Cloudron` - When checked, users from the external directory will be automatically created on Cloudron and kept up-to-date.

* `Sync Groups` - When enabled, groups will be automatically created and users will be associated. Currently, groups removed from the external source will not be deleted on the Cloudron during a sync.

!!! note "No automatic sync"
    For the moment, user synchronisation has to be **manually** triggered from within the dashboard using the **Synchronize** button.

### Cloudron

To use another Cloudron as the external LDAP directory, do the following:

* Enable `Directory Server` in the `Users` view of the other Cloudron. Be sure to white list this Cloudron
  and specify a secure secret.

* On this Cloudron, select `Cloudron` as the provider.

<center>
<img src="/img/users-external-ldap-cloudron.png" class="shadow" width="500px">
</center>

### JumpCloud

The following screenshot shows the available configure options using a jumpcloud external LDAP directory:

* `Server URL`: `ldaps://ldap.jumpcloud.com:636`
* `Base DN`: `ou=users, o=3214565, dc=jumpcloud, dc=com`
* `Filter`: `(objectClass=inetorgperson)`
* `Bind DN`: `uid=ldap_admin,ou=Users,o=3214565,dc=jumpcloud,dc=com`
* `Bind password`: `admin password`
* `Username field`: `uid`

<center>
<img src="/img/users-external-ldap-jumpcloud.png" class="shadow" width="500px">
</center>

### Okta

To use the Okta integration, do the following:

* In Okta, enable the [LDAP interface](https://help.okta.com/en/prod/Content/Topics/Directory/LDAP_Using_the_LDAP_Interface.htm).
  You can do this from the `Directory Integrations` page.

* By default, Okta uses email as the default uid. Cloudron requires usernames for LDAP integration to work. If you already have a
  field in Okta that can provide usernames, provide that as the `username field`. If not, you can create a new field in the
  profile editor and set that.

* Cloudron configuration (replace 'org' below):
    * `Server URL`: `ldaps://<org>.ldap.okta.com`
    * `Base DN`: `ou=users, dc=<org>, dc=okta, dc=com`
    * `Filter`: `(objectClass=inetorgperson)`
    * `Bind DN`: `uid=<admin>, dc=<org>, dc=okta, dc=com`
    * `Bind password`: `admin password`
    * `Username field`: see above

<center>
<img src="/img/users-external-ldap-okta.png" class="shadow" width="500px">
</center>

## OpenID Connect

[OpenID Connect 1.0](https://openid.net/connect/) is a simple identity layer on top of the OAuth 2.0 protocol.

Cloudron is an OIDC provider. The main advantages the OpenID integration offers over LDAP are:
* True single sign-on across apps. Once logged into the main dashboard, users can automatically login to apps.
* Manage app sessions from the Dashboard
* 2FA support across apps
* More secure since apps never see the user's password

Apps integrate automatically with the OIDC server using the [oidc addon](/packaging/addons/#oidc).

### Endpoints

| Name | URL |
|------|------ |
| Discovery URL 	| `https://my.cloudron.example/.well-known/openid-configuration` and `https://my.cloudron.example/openid/.well-known/openid-configuration`  |
| Auth Endpoint 	| `https://my.cloudron.example/openid/auth` |
| Token Endpoint 	| `https://my.cloudron.example/openid/token` |
| Keys Endpoint 	| `https://my.cloudron.example/openid/jwks` |
| Profile Endpoint 	| `https://my.cloudron.example/openid/me` |
| Logout URL 	    | `https://my.cloudron.example/openid/session/end` |

### Scopes and Claims

For most clients, it is recommend to add `openid` (mandatory), `profile` and `email` scope. Scopes are space separated.
This will ensure the app has access to the user profile claims.

Cloudron currently provides the following scopes and corresponding claims:

| Scope | Claim |
|------|------ |
| `profile` | `family_name`, `given_name`, `locale` (always `en-US`), `name`, `preferred_username` |
| `email` | `email`, `email_verified` |

On Cloudron, the `sub` property (the unique user identifier) is the **username**.

### OIDC Clients

OIDC clients can be managed in the `Users` view. To create a new client, provide the `clientid`, `clientsecret` and the callback URL.

<center>
<img src="/img/users-oidc-client-add.png" class="shadow" width="500px">
</center>

Then, in the client app, configure the provider as follows:

<center>
<img src="/img/users-oidc-client-config.png" class="shadow" width="500px">
</center>

