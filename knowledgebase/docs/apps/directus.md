# <img src="/img/directus-logo.png" width="25px"> Directus App

## About

Directus is an Instant App & API for your SQL Database.

* Questions? Ask in the [Cloudron Forum - Directus](https://forum.cloudron.io/category/101/directus)
* [Directus Website](https://directus.io)
* [Directus issue tracker](https://github.com/directus/directus/issues)

## Environment variables

[Custom environment variables](https://github.com/directus/directus/blob/main/api/example.env) can be set
in `/app/data/env.sh` using the [File manager](/apps/#file-manager).

Be sure to restart the app after making any changes.

## Login

### Cloudron Directory

By default, local users (including the auto-created admin user) are allowed to login along with Cloudron Directory users. To disable this, set the following in `/app/data/env.sh` using the [File manager](/apps/#file-manager) and restart the app.

```
export AUTH_DISABLE_DEFAULT="true" # no local user login
```

By default, Cloudron users are given the built-in `Adminstrator` role. You can create another role and assign this as the default instead. To do so, set the following in `/app/data/env.sh` using the [File manager](/apps/#file-manager) and restart the app.

```
export AUTH_LDAP_DEFAULT_ROLE_ID=<id of role>
```

## Custom modules

To install custom modules for an extension:

* Install the modules using `npm` in the extension's subdirectory.
* Then, edit `/app/data/env.sh` using the [File manager](/apps/#file-manager) and add export `NODE_PATH=/app/data/extensions/myext/node_modules`
* Restart the app.

!! note "Multiple module paths"
    `NODE_PATH` sets the search directory for node modules. You can set multiple paths separated by colon (similar to `PATH`).

