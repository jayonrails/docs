# <img src="/img/statping-logo.png" width="25px"> Statping App

!!! warning "Discontinued"
    Please note this app is not available anymore since upstream development has stopped.

## About

Statping is a status Page for monitoring your websites and applications with beautiful graphs, analytics, and plugins.

* Questions? Ask in the [Cloudron Forum - Statping](https://forum.cloudron.io/category/92/statping)
* [Statping Website](https://statping.com/)
* [Statping issue tracker](https://github.com/statping/statping/issues)

## Config changes

Sometimes after making config or setting changes, the graphs do not appear.
To fix this, go to admin settings and clear the cache after doing any config or setting
changes.

