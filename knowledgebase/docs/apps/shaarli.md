# <img src="/img/shaarli-logo.png" width="25px"> Shaarli App

## About

The personal, minimalist, super-fast, database free, bookmarking service.

* Questions? Ask in the [Cloudron Forum - Shaarli](https://forum.cloudron.io/category/82/shaarli)
* [Shaarli Website](https://github.com/shaarli/Shaarli)
* [Shaarli Docs](https://shaarli.readthedocs.io/en/master/)
* [Shaarli issue tracker](https://github.com/shaarli/Shaarli/issues)

## Browser Extensions

Shaarli's browser extensions currently [do not work](https://chrome.google.com/webstore/detail/shiny-shaarli/hajdfkmbdmadjmmpkkbbcnllepomekin/support) on Cloudron because Cloudron sets the `X-Frame-Options` as
a security measure. A future version of Cloudron might support disabling
this security measure.

## Using the bookmarklet

* Open your Shaarli and Login
* Click the Tools button in the top bar
* Drag the `✚Shaare link` button, and drop it to your browser's bookmarks bar.

You can read more about setting up the bookmarklet in the [Shaarli docs](https://shaarli.readthedocs.io/en/master/Bookmarklet/)

