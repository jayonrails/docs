# <img src="/img/kanboard-logo.png" width="25px"> Kanboard App

## About

Kanboard is a free and open source Kanban project management software.

* Questions? Ask in the [Cloudron Forum - Kanboard](https://forum.cloudron.io/category/36/kanboard)
* [Kanboard Website](http://kanboard.org/)
* [Kanboard issue tracker](https://github.com/kanboard/kanboard/issues)

## Installing plugins

[Kanboard Plugins](https://kanboard.org/#plugins) are used to extend Kanboard. Kanboard has a UI to install and uninstall plugins
at `/app/data/plugins` directly.

Plugins can also be installed manually at `/app/data/plugins`. Use the [file manager](/apps/#file-manager) to upload
and extract plugins under that directory.

## Custom configuration

Custom plugin configuration can be stored in `/app/data/customconfig.php`. To edit
the file, use the [file manager](/apps#file-manager) for the app.

