# <img src="/img/surfer-logo.png" width="25px"> Surfer App

## About

Surfer is a simple static file server.

* Questions? Ask in the [Cloudron Forum - Surfer](https://forum.cloudron.io/category/28/surfer)
* [Surfer repo](https://git.cloudron.io/cloudron/surfer)
* [Surfer issue tracker](https://git.cloudron.io/cloudron/surfer/-/issues)

## Admin page

The web interface is available under the `https://[appdomain]/_admin/` location.

## Managing files

There are 4 ways to manage files in surfer:

* [Web interface](#web-interface)
* [CLI tool](#cli-tool)
* [WebDAV](#webdav) endpoint to manage files in your local file manager.
* [SFTP](#sftp)

### Web interface

Files and folders can be uploaded via the web interface located at `/_admin`.

<center>
<img src="/img/surfer-web-interface.png" class="shadow" width="500px">
</center>

### CLI Tool

The Surfer cli tool can be installed using npm.

```
npm -g install cloudron-surfer
```

Login using an API access token created in the surfer admin interface:

```
surfer config --server <this app's domain> --token
```

Put some files:

```
surfer put index.html favicon.ico /
```

Put a directory (the `/.` below meand that the contents of `build` dir get copied into the root of surfer.
Without it, a `build` directory will get created in the root of surfer).

```
surfer put build/. /
```

To get help:

```
$ surfer
Usage: surfer [options] [command]

Options:
  -V, --version                output the version number
  -h, --help                   display help for command

Commands:
  login                        Set default server
  logout                       Unset default server
  config|configure [options]   Configure default server
  put [options] <file|dir...>  Uploads a list of files or dirs to the destination. The last argument is destination dir
  get [options] [file|dir]     Get a file or directory listing
  del [options] <file>         Delete a file or directory
  help [command]               display help for command
```

### WebDAV

[WebDAV](https://en.wikipedia.org/wiki/WebDAV) is a well supported extension of the Hypertext Transfer Protocol that allows clients to perform remote Web content authoring operations. WebDAV shares can be mounted usually with your local file manager.

The URI schemes differ on the common platforms:

| Platform| URI |
| ---     | --- |
| Windows | `https://[appdomain]/_webdav/` |
| Mac     | `https://[appdomain]/_webdav/` |
| Gnome   | `davs://[appdomain]/_webdav/` |
| KDE     | `webdavs://[appdomain]/_webdav/` |

On Linux the [Davfs2](http://savannah.nongnu.org/projects/davfs2) library can also be used to locally mount a share:

```
mount -t davfs https://[appdomain]/_webdav/ /mount/point
```

### SFTP

Files can be uploaded using an SFTP client like [FileZilla](https://filezilla-project.org/). See [SFTP access](/apps/#ftp-access) for login details.

!!! note "SFTP Access"
    SFTP access for non-admin users can be granted using the [access control UI](/apps/#restricting-app-access-to-specific-users).

## Folder structure

By default, any `index.html` or `index.htm` file is served up. This file name can be changed in the `Settings` page.

If Public Folder Listing is enabled, the directory contents is listed, provided the directory has no index page.

If a `404.html` is present in the root directory, it will be served up for missing pages.

## Access Control

Access to the site can be controlled from the `Settings` page. There are 3 options:

* Public (everyone) - anyone can view the site
* Password restricted - anyone with a password can view the site
* User restricted - only users with a Cloudron login can view the site

<center>
<img src="/img/surfer-access-control.png" class="shadow" width="500px">
</center>


## CI/CD integration

You can setup your CI/CD to automatically push static files to surfer using the [CLI](#cli-tool) as follows:

* First, create an `Access Token` in surfer from the `Settings` menu.

* Install the surfer cli tool as part of the CI/CD pipeline.

* Push the artifacts (`dist/` in the example below):

```
surfer put --token api-7e6d90ff-5825-4ebe-a85b-a68795055955 --server surfer.cloudron.ml dist/. /
```

