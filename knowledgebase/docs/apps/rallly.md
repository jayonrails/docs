# <img src="/img/rallly-logo.png" width="25px"> Rallly App

## About

Self-hostable doodle poll alternative. Find the best date for a meeting with your colleagues or friends without the back and forth emails.

* Questions? Ask in the [Cloudron Forum - Rallly](https://forum.cloudron.io/category/154/rallly)
* [Rallly Website](https://rallly.co)
* [Rallly issue tracker](https://github.com/lukevella/rallly/issues)
