# <img src="/img/calcom-logo.png" width="25px"> Cal.com App

## About

Cal.com is the event-juggling scheduler for everyone. Focus on meeting, not making meetings. Free for individuals.

* Questions? Ask in the [Cloudron Forum - Cal.com](https://forum.cloudron.io/category/174/cal-com)
* [Cal.com Website](https://cal.com/)
* [Cal.com Docs](https://cal.com/docs)
* [Cal.com Issue Tracker](https://github.com/calcom/cal.com/issues)
