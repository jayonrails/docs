# <img src="/img/loomio-logo.png" width="25px"> Loomio App

## About

Loomio is a collaborative decision making tool.

* Questions? Ask in the [Cloudron Forum - Loomio](https://forum.cloudron.io/category/160/loomio)
* [Loomio Website](https://www.loomio.com)
* [Loomio issue tracker](https://github.com/loomio/loomio/issues)

## Custom config

Custom configuration can be added by editing `/app/data/env.sh` using the [File manager](/apps/#file-manager).

See [this file](https://github.com/loomio/loomio-deploy/blob/master/scripts/default_env) for a list of supported
options.

Be sure to restart the app after making any changes.

## Registration

By default, registration is enabled and new users can create groups. You can adjust this using the following variables
in `/app/data/env.sh`:

```
export FEATURES_DISABLE_CREATE_USER=1     # users must be invited
export FEATURES_DISABLE_CREATE_GROUP=1    # users cannot create groups
export FEATURES_DISABLE_PUBLIC_GROUPS=1   # disable /explore
export FEATURES_DISABLE_HELP_LINK=1       # disable the help link
export FEATURES_DISABLE_EMAIL_LOGIN=1     # Disable login via email (usually when you have enabled SSO of some kind)
```

