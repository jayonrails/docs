# <img src="/img/tinytinyrss-logo.png" width="25px"> Tiny Tiny RSS

## About

Tiny Tiny RSS is a free and open source web-based news feed (RSS/Atom) reader and aggregator

* Questions? Ask in the [Cloudron Forum - Tiny Tiny RSS](https://forum.cloudron.io/category/21/tiny-tiny-rss)
* [Tiny Tiny RSS Website](http://tt-rss.org)
* [Tiny Tiny RSS forum](https://community.tt-rss.org/)

## Customizing configuration

Use the [File Manager](/apps#file-manager) to place custom configuration under `/app/data/env.sh`.

Please be careful when changing configuration since the Cloudron packaging might depend on it.

## Installing Plugins

To install [plugins](https://tt-rss.org/wiki/Plugins), simply extract
them using the [File Manager](/apps#file-manager) to `/app/data/plugins.local` and restart the app.

To enable the plugin globally, you must edit the `TTRSS_PLUGINS` variable in `/app/data/env.sh`.
Alternately, each user can enable the plugin individually under Preferences.

You can see list of plugins [here](https://gitlab.tt-rss.org/tt-rss/plugins)

## Installing themes

To install [themes](https://tt-rss.org/wiki/Themes), simply extract them 
using the [File Manager](/apps#file-manager) to `/app/data/themes.local` and restart the app.

Some suggested [themes](https://tt-rss.org/wiki/Themes):

* [Reeder](https://github.com/tschinz/tt-rss_reeder_theme)
* [Clean GReader](https://github.com/naeramarth7/clean-greader)
* [Feedly](https://github.com/levito/tt-rss-feedly-theme)

## Fever support

TinyTinyRSS supports Fever API using a plugin. There are many version of 
fever API floating around but [this version](https://github.com/wodev/tinytinyrss-fever-plugin#installation)
is known to work.

Instruction on how to setup the apps is in the [old forum page](https://tt-rss.org/oldforum/viewtopic.php?f=22&t=1981)

## External registration

To enable external registration, make two changes to `/app/data/env.sh` using the [File Manager](/apps#file-manager):

* Set `TTRSS_ENABLE_REGISTRATION` to `true`
* Edit the `TTRSS_PLUGINS` variable to include `auth_internal`. Note that `TTRSS_PLUGINS` is comma separated plugin names.
  This enables auth via the internal database authentication in addition to LDAP that Cloudron setup.

