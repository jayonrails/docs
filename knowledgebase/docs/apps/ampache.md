# <img src="/img/ampache-logo.png" width="25px"> Ampache App

## About

Ampache is a web based audio/video streaming application and file manager. Allowing you to access your music & videos
from anywhere, using almost any internet enabled device.

* Questions? Ask in the [Cloudron Forum - Ampache](https://forum.cloudron.io/category/110/ampache)
* [Ampache Website](http://www.ampache.org)
* [Ampache issue tracker](https://github.com/ampache/ampache/issues)

## Import Audio Files

Ampache works with audio catalogs. To  import songs into Ampache:

* Create a directory for it through the [File Manager](/apps#file-manager). For example create `/app/data/catalogs/music`.
You can upload new audio files to this folder now or later.
* Create a new catalog. You can do this by clicking the `Admin` icon (next to the gear icon).
  <img src="/img/ampache-catalog.png" class="shadow" width="500px">
* Set the catalog type to `local`
* Set the directory path `/app/data/catalogs/music` in the path text input
* Trigger a catalog update via the ampache UI to update the catalog.


## Customization

Use the [Web terminal](/apps#web-terminal) or the [File Manager](/apps#file-manager)
to edit custom configuration under `/app/data/config/ampache.cfg.php`. Full documentation
is available [here](https://github.com/ampache/ampache/wiki/Basic).

## Playback issues

For larger audio files, ampache often needs more memory to play them.
Unlike other php based apps, increasing the memory limit in php.ini will get overwritten.
The correct file for ampache is `/app/data/config/ampache.cfg.php`:
```
memory_limit = 512M
```

## CLI

[Ampache CLI](https://github.com/ampache/ampache/wiki/CLI) can be used to run various
common management tasks. You can use the CLI using the [Web terminal](/apps#web-terminal)
as follows:

```
    sudo -u www-data /usr/bin/php /app/code/bin/install/catalog_update.inc
```

## Features

Ampache has a lot of exciting features that are worth exploring further:

* [Subscribe to Podcasts](https://github.com/ampache/ampache/wiki/Podcasts)
* [Icecast compatible Channels](https://github.com/ampache/ampache/wiki/Channels)
* [Democratic play](https://github.com/ampache/ampache/wiki/Democratic)
* [Subsonic API](https://github.com/ampache/ampache/wiki/subsonic)

