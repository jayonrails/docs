# <img src="/img/prometheus-logo.png" width="25px"> Prometheus App

## About

Prometheus is an open-source systems monitoring and alerting toolkit.

* Questions? Ask in the [Cloudron Forum - Ackee](https://forum.cloudron.io/category/125/ackee)
* [Prometheus website](https://prometheus.io/)
* [Prometheus docs](https://prometheus.io/docs/introduction/overview/)
* [Prometheus support](https://prometheus.io/support-training/)
* [Prometheus issue tracker](https://github.com/prometheus/prometheus/issues)

## CLI Args

Various [operations aspects](https://prometheus.io/docs/prometheus/latest/storage/#operational-aspects) can be
set in the command line. To adjust Prometheus command line options, edit `/app/data/env.sh` using the [File manager](/apps/#file-manager)
and adjust `cli_options` variable as needed. For example:

```
export cli_options="--storage.tsdb.retention.time=25d --storage.tsdb.path=/app/data/storage"
``

Be sure to restart the app after making any changes.

