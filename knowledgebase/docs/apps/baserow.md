# <img src="/img/baserow-logo.png" width="25px"> Baserow App

## About

Baserow is an open source no-code database tool and Airtable alternative.

* Questions? Ask in the [Cloudron Forum - Baserow](https://forum.cloudron.io/category/136/baserow)
* [Baserow website](https://baserow.io/)
* [Baserow community](https://community.baserow.io/)
* [Baserow issue tracker](https://gitlab.com/bramw/baserow/-/issues)

## Custom configuration

Custom env variables can be set in the file `/app/data/env.sh` using the [File manager](/apps/#file-manager).
Be sure to restart the app after making any changes.

