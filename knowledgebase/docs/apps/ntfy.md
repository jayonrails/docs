# <img src="/img/ntfy-logo.png" width="25px"> ntfy App

## About

ntfy lets you send push notifications to your phone or desktop via scripts from any computer, using simple HTTP PUT or POST requests. I use it to notify myself when scripts fail, or long-running commands complete.

* Questions? Ask in the [Cloudron Forum - ntfy](https://forum.cloudron.io/category/148/piwigo)
* [ntfy Website](https://ntfy.sh/)
* [ntfy Docs](https://docs.ntfy.sh/)
* [ntfy issue tracker](https://github.com/binwiederhier/ntfy/issues)

## Custom config

[Custom configuration](https://ntfy.sh/docs/config/) can be set in `/app/data/config/server.yml` using the [File manager](/apps/#file-manager).

## CLI

To access the ntfy CLI, simply open a  [Web terminal](/apps/#web-terminal) and run `ntfy`:

```
root@89c61cad-e48d-4fce-bcc2-b82b3b1b8a69:/app/code# ntfy
NAME:
   ntfy - Simple pub-sub notification service

USAGE:
   ntfy [OPTION..]

COMMANDS:
...

```

CLI configuration can be adjusted in `/app/data/config/client.yml`.

## Access control

ntfy's [access control](https://docs.ntfy.sh/config/?#access-control) mechanism can be used to configure authentication and authorization.

By default, Cloudron configures ntfy as a [private instance](https://docs.ntfy.sh/config/?#example-private-instance). `auth-default-access` is set to `deny-all`.

!!! note "Web interface"
    The web interface is open to all and can be used to send/receive notifications from one or more ntfy servers. Keeping the UI unprotected is harmless as
    it's purely a frontend application and one cannot send and receive notifications without authentication.

## Change password

To change the password of a user, use the [Web terminal](/apps/#web-terminal):

```
# ntfy user change-pass admin
changed password for user admin
```

