# <img src="/img/miniflux-logo.png" width="25px"> Miniflux App

## About

Miniflux is a minimalist and opinionated feed reader.

* Questions? Ask in the [Cloudron Forum - Miniflux](https://forum.cloudron.io/category/144/miniflux)
* [Miniflux Website](https://miniflux.app/)
* [Miniflux docs](https://miniflux.app/docs/index.html)
* [Miniflux issue tracker](https://github.com/miniflux/v2/issues)

## Reset password

To reset the password, open a [Web terminal](/apps/#web-terminal):

```
# export DATABASE_URL="${CLOUDRON_POSTGRESQL_URL}?sslmode=disable"
# /app/code/miniflux -reset-password
```

## Custom variables

Custom environment variables can be set in `/app/data/env.sh` using the [File manager](/apps/#file-manager).
Be sure to restart the app after making any changes.

```
export FETCH_YOUTUBE_WATCH_TIME=1
```

See [upstream docs](https://miniflux.app/docs/configuration.html) for the various configuration options.
