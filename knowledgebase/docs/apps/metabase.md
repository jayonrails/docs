# <img src="/img/metabase-logo.png" width="25px"> Metabase App

## About

Metabase is the simplest, fastest way to get business intelligence and analytics to everyone in your company.

* Questions? Ask in the [Cloudron Forum - Metabase](https://forum.cloudron.io/category/86/metabase)
* [Metabase Website](https://www.metabase.com)
* [Metabase forum](https://discourse.metabase.com/)
* [Metabase issue tracker](https://github.com/metabase/metabase/issues)

## Reset password

To reset the password of an admin with the email `admin@cloudron.local`, open a [Web terminal](/apps/#web-terminal):

```
export MB_DB_TYPE="postgres"
export MB_DB_HOST=${CLOUDRON_POSTGRESQL_HOST}
export MB_DB_PORT=${CLOUDRON_POSTGRESQL_PORT}
export MB_DB_USER=${CLOUDRON_POSTGRESQL_USERNAME}
export MB_DB_PASS=${CLOUDRON_POSTGRESQL_PASSWORD}
export MB_DB_DBNAME=${CLOUDRON_POSTGRESQL_DATABASE}
java -jar metabase.jar reset-password admin@cloudron.local
```

This will produce an output that contains the token:

```
...
OK [[[1_63d77453-846f-44ca-939e-7aa0fa09b870]]]
```

Navigate to `https://metabase.example.com/auth/reset_password/1_63d77453-846f-44ca-939e-7aa0fa09b870` to
reset the password. Be sure to replace the metabase domain name and the token with your own.

