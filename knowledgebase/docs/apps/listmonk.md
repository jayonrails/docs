# <img src="/img/listmonk-logo.png" width="25px"> Listmonk App

## About

Listmonk is a standalone, self-hosted, newsletter and mailing list manager. It is fast, feature-rich, and packed into a single binary.

* Questions? Ask in the [Cloudron Forum - Listmonk](https://forum.cloudron.io/category/145/listmonk)
* [Listmonk Website](https://listmonk.app/)
* [Listmonk issue tracker](https://github.com/knadh/listmonk/issues/)

## Login

The login credentials are stored in `/app/data/env.sh`. Please edit the following section:

```
export LISTMONK_app__admin_username=admin
export LISTMONK_app__admin_password=changeme
```

Be sure to restart the app, after making any changes.

## Timezone

To set the timezone, edit the `TZ` variable inside `/app/data/env.sh` using the [File manager](/apps/#file-manager).

Be sure to restart the app, after making any changes.

## Static templates

[System templates](https://listmonk.app/docs/templating/#system-templates) are used for rendering public user facing pages such as the subscription management page, and in automatically generated system e-mails such as the opt-in confirmation e-mail.

The original template files, matching the upstream version, are located at `/app/pkg/static.template`.

You can make a copy of those into `/app/data/static` using the [Web terminal](/apps#web-terminal) to run the following command:
```
cp -rf /app/pkg/static.template/* /app/data/static/
```
Then restart the app to make it pickup the changes. A restart is required everytime files in that folder are changed.

!!! warning "Version incompatibilities"
    Since the template files might depend on specific listmonk versions, any app update might break due to incompatibilites.
    If custom templates are used, it may be good to disable automatic app updates and check functionality after each manually applied update.

## i18n

[Additional languages](https://listmonk.app/docs/i18n/) can be uploaded to `/app/data/i18n` using the [File manager](/apps/#file-manager).

