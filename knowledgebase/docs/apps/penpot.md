# <img src="/img/penpot-logo.png" width="25px"> Penpot App

## About

Penpot is the first Open Source design and prototyping platform meant for cross-domain teams.
Non dependent on operating systems, Penpot is web based and works with open standards (SVG).
Penpot invites designers all over the world to fall in love with open source while getting developers excited about the design process in return.

* Questions? Ask in the [Cloudron Forum - Penpot](https://forum.cloudron.io/category/166/penpot)
* [Penpot Website](https://penpot.app/)
* [Penpot Community](https://community.penpot.app/)
