# <img src="/img/linkding-logo.png" width="25px"> Linkding App

## About

Linkding is a bookmark manager that you can host yourself. It's designed be to be minimal, fast, and easy to set up.

* Questions? Ask in the [Cloudron Forum - Linkding](https://forum.cloudron.io/category/170/linkding)
* [Linkding GitHub](https://github.com/sissbruecker/linkding)
* [Linkding issue tracker](https://github.com/sissbruecker/linkding/issues)

## Customization

Linkding has various [options](https://github.com/sissbruecker/linkding/blob/master/docs/Options.md).

To customize, edit `/app/data/env.sh` using the [File manager](/apps/#file-manager). Be sure to restart the app after
making any changes.

