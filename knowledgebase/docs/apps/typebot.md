# <img src="/img/typebot-logo.png" width="25px"> Typebot App

## About

Typebot is a visual chatbot builder that helps you create chatbots for your website without coding.

* Questions? Ask in the [Cloudron Forum - Typebot](https://forum.cloudron.io/category/173/typebot)
* [Typebot Website](https://typebot.io/)
* [Typebot Issues](https://github.com/baptisteArno/typebot.io/issues)

## Custom Configuration

Typebot supports various [configuration](https://docs.typebot.io/self-hosting/configuration)
parameters that can be set as environment variables. You can edit `/app/data/env.sh` using
the [File Manager](/apps/#file-manager) to set custom configuration.

For example:

```
export NEXT_PUBLIC_UNSPLASH_APP_NAME = Whatever_Name_I_Gave_This_App_On_Unsplash	
export NEXT_PUBLIC_UNSPLASH_ACCESS_KEY = theRandomStringThatUnsplashGaveAsAnAPIkey
```

Be sure to restart the app after making any changes.

